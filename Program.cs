﻿using static System.Console;
using System.Diagnostics;
namespace Snake
{
    class SnakeGame
    {
        static void Main(string[] args)
        {
            Console.WindowHeight = 16;
            Console.WindowWidth = 32;

            // Game refresh rate in miliseconds
            int gameRefreshRateMS = 500;

            // Score should start with 0
            int score = 0;
            // Random generator for berry placing
            Random randomGenerator = new();
            bool gameover = false;

            Pixel snakeHead = new()
            {
                XPos = WindowWidth / 2,
                YPos = WindowHeight / 2,
                ScreenColour = ConsoleColor.Red
            };

            // There should be specifically said how long the snake
            // should in the beginning
            int startSnakeLength = 5;
            // Start with moving right
            Direction movement = Direction.Right;
            List<int> snakeBodyPosX = [];
            List<int> snakeBodyPosY = [];
            
            // Fixed berry spawning in wall in the beginning
            int berryPosX = randomGenerator.Next(1, WindowWidth - 1);
            int berryPosY = randomGenerator.Next(1, WindowHeight - 1);
            
            // Define characters for each object here:
            // This really makes the code more readable
            // and also makes changes easier
            string snakeHeadCharacter = "☻";
            string snakeBodyCharacter = "■";
            string borderCharacter = "■";
            string berryCharacter = "■";
        
            while (true)
            {
                Console.Clear();
                
                if (snakeHead.XPos == WindowWidth - 1 || snakeHead.XPos == 0 || snakeHead.YPos == WindowHeight - 1 || snakeHead.YPos == 0)
                {
                    gameover = true;
                }
                
                DrawBorder(WindowWidth, WindowHeight, borderCharacter);
                ForegroundColor = ConsoleColor.Green;
                
                // If berry is eaten add score and set berry
                // position at a new random integers within game field
                if (berryPosX == snakeHead.XPos && berryPosY == snakeHead.YPos)
                {
                    score++;
                    berryPosX = randomGenerator.Next(1, WindowWidth - 2);
                    berryPosY = randomGenerator.Next(1, WindowHeight - 2);
                }

                // Print snake body and check for body/head colision
                for (int i = 0; i < snakeBodyPosX.Count(); i++)
                {
                    SetCursorPosition(snakeBodyPosX[i], snakeBodyPosY[i]);
                    Write(snakeBodyCharacter);
                    if (snakeBodyPosX[i] == snakeHead.XPos && snakeBodyPosY[i] == snakeHead.YPos)
                    {
                        gameover = true;
                    }
                }

                if (gameover)
                {
                    break;
                
                }

                SetCursorPosition(snakeHead.XPos, snakeHead.YPos);
                ForegroundColor = snakeHead.ScreenColour;
                Write(snakeHeadCharacter);
                SetCursorPosition(berryPosX, berryPosY);
                ForegroundColor = ConsoleColor.Cyan;
                Write(berryCharacter);
                
                var stopWatch = Stopwatch.StartNew();

                while (stopWatch.ElapsedMilliseconds <= gameRefreshRateMS)
                {
                    // Read movement only when key was pressed -> Console.KeyAvailable
                    if (KeyAvailable)
                    {
                        ReadPlayerMovement(ref movement);
                    }
                }
                snakeBodyPosX.Add(snakeHead.XPos);
                snakeBodyPosY.Add(snakeHead.YPos);
                switch (movement)
                {
                    case Direction.Up:
                        snakeHead.YPos--;
                        break;
                    case Direction.Down:
                        snakeHead.YPos++;
                        break;
                    case Direction.Left:
                        snakeHead.XPos--;
                        break;
                    case Direction.Right:
                        snakeHead.XPos++;
                        break;
                }
                // This part deletes any pixel left behind
                // that would make the snake longer than it should be
                // E.g. increasing score by eating berry makes you grow bigger
                if (snakeBodyPosX.Count() > startSnakeLength + score)
                {
                    snakeBodyPosX.RemoveAt(0);
                    snakeBodyPosY.RemoveAt(0);
                }
            }
            SetCursorPosition(WindowWidth / 5, WindowHeight / 2);
            WriteLine("Game over, Score: "+ score);
            SetCursorPosition(WindowWidth / 5, WindowHeight / 2 +1);
        }
        private static void DrawBorder(int screenWidth, int screenHeight, string borderCharacter)
        {
            for (int i = 0; i < screenWidth; i++)
            {
                SetCursorPosition(i, 0);
                Write(borderCharacter);
                SetCursorPosition(i, screenHeight - 1);
                Write(borderCharacter);
            }
            for (int i = 0; i < screenHeight; i++)
            {
                SetCursorPosition(0, i);
                Write(borderCharacter);
                SetCursorPosition(screenWidth - 1, i);
                Write(borderCharacter);
            }
        }

        // Reads the pressed key
        private static void ReadPlayerMovement(ref Direction movement)
        {
            ConsoleKeyInfo toets = ReadKey(true);
            // We have to check if the snake is not going opposite direction.
            // We do not allow 180 degrees turn
            if (toets.Key.Equals(ConsoleKey.UpArrow) && movement != Direction.Down)
            {
                movement = Direction.Up;
            }
            else if (toets.Key.Equals(ConsoleKey.DownArrow) && movement != Direction.Up)
            {
                movement = Direction.Down;

            }
            else if (toets.Key.Equals(ConsoleKey.LeftArrow) && movement != Direction.Right)
            {
                movement = Direction.Left;
            }
            else if (toets.Key.Equals(ConsoleKey.RightArrow) && movement != Direction.Left)
            {
                movement = Direction.Right;
            }
        }

        class Pixel
        {
            public int XPos { get; set; }
            public int YPos { get; set; }
            public ConsoleColor ScreenColour { get; set; }
        }
        enum Direction
        {
            Up,
            Down,
            Right,
            Left
        }
    }
}
//¦
